/*
* An helper namespace that contains opcodes
*/
namespace Pulp.Compiler {

    public class Repository {
        public static OpCode NOP = new OpCode (0x00, "Do Nothing (No OPeration).");

        public static OpCode POP = new OpCode (0x01, "Pop TS off of the value stack.");
        public static OpCode ROT_2 = new OpCode (0x02, "TS, TS1 => TS1, TS");
        public static OpCode ROT_3 = new OpCode (0x03, "TS, TS1, TS2 => TS2, TS, TS1");
        public static OpCode DUP_TOP = new OpCode (0x04, "Duplicate TS.");
        public static OpCode LOAD_CONST = new OpCode (0x0a, "TS = ConstantsTable[ARG].", true);

        public static OpCode NEW_ENV = new OpCode (0x20, "Push a new scope onto the environment.");
        public static OpCode POP_ENV = new OpCode (0x21, "Exit scope: pops it off of the environment.");
        public static OpCode LET = new OpCode (0x25, "Binds SymTable[ARG] to TS in local scope.", true);
        public static OpCode LET_POP = new OpCode (0x26, "LET ARG and POP", true);
        public static OpCode STORE = new OpCode (0x27, "Lookup for a scope containing SymbolsTable[ARG] & update the binding with TS", true);
        public static OpCode STORE_POP = new OpCode (0x28, "STORE ARG and POP", true);
        public static OpCode LOAD = new OpCode (0x29, "Lookup SymbolsTable[ARG] in the environment and push", true);

        public static OpCode ADD = new OpCode (0x30, "TS = TS1 + TS");
        public static OpCode SUB = new OpCode (0x31, "TS = TS1 - TS");
        public static OpCode MUL = new OpCode (0x32, "TS = TS1 * TS");
        public static OpCode DIV = new OpCode (0x33, "TS = TS1 / TS");
        public static OpCode POW = new OpCode (0x34, "TS = TS1 ** TS");
        public static OpCode MOD = new OpCode (0x35, "TS = TS1 % TS");

        public static OpCode BIT_OR = new OpCode (0x36, "TS = TS1 | TS");
        public static OpCode BIT_AND = new OpCode (0x37, "TS = TS1 & TS");
        public static OpCode BIT_XOR = new OpCode (0x38, "TS = TS1 ^ TS");
        public static OpCode LSHIFT = new OpCode (0x39, "TS = TS1 << TS");
        public static OpCode RSHIFT = new OpCode (0x3a, "TS = TS1 >> TS");

        public static OpCode UNARY_MINUS = new OpCode (0x40, "TS = -TS");
    }
}
