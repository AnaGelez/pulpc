using Gee;
using Pulp.Compiler.AST;

namespace Pulp.Compiler {

    /*
    * Creates an AST from tokens.
    */
    public class Parser : Object {

        /*
        * Creates an AST
        *
        * @return A {@link Global}
        */
        public Expression parse (ArrayList<Token> tokens) {
            Global global = new Global ();
            Expression? prev_expr = null;

            for (int i = 0; i < tokens.size; i++) {
                Token current_token = tokens[i];
                Token? previous_token = i > 0 ? tokens[i - 1] : null;
                Token? next_token = i < (tokens.size - 1) ? tokens[i + 1] : null;

                if (current_token.category == "operator.add") {
                    if (previous_token == null || next_token == null) {
                        log_error ("Unexpected token '+'.", current_token.line, current_token.column);
                    } else if (previous_token.category != "literal.number" || next_token.category != "literal.number") {
                        log_error ("Additions can only be done on literal integer yet.", current_token.line, current_token.column);
                    } else {
                        BinaryOperator add = new BinaryOperator () {
                            operator = "+",
                            left = prev_expr
                        };
                        prev_expr = add;
                    }
                } else if (current_token.category == "operator.sub") {
                    if (previous_token == null || next_token == null) {
                        log_error ("Unexpected token '-'.", current_token.line, current_token.column);
                    } else if (previous_token.category != "literal.number" || next_token.category != "literal.number") {
                        log_error ("Substractions can only be done on literal integer yet.", current_token.line, current_token.column);
                    } else {
                        BinaryOperator sub = new BinaryOperator () {
                            operator = "-",
                            left = prev_expr
                        };
                        prev_expr = sub;
                    }
                } else if (current_token.category == "literal.number") {
                    if (prev_expr != null && prev_expr.get_type () == typeof (BinaryOperator)) {
                        ((BinaryOperator)prev_expr).right = new Number () { value = int64.parse (current_token.lexeme) };
                    } else {
                        prev_expr = new Number () { value = int64.parse (current_token.lexeme) };
                    }
                } else if (current_token.category == "literal.string") {
                    log_warning ("Strings aren't supported yet.", current_token.line, current_token.column);
                } else if (current_token.category == "expression.end") {
                    global.children.add (prev_expr);
                    prev_expr = null;
                } else {
                    log_warning ("Unknown token type : '%s'".printf (current_token.category), current_token.line, current_token.column);
                }
            }

            return global;
        }

    }
}
