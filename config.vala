namespace Pulp.Compiler {

    const int VERSION_MAJOR = 0;
    const int VERSION_MINOR = 1;
    const int VERSION_PATCH = 0;

}
