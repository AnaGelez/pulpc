namespace Pulp.Compiler {
    public class OpCode : Object {

        public string documentation { get; set; }

        public uint8 value { get; set; }

        private bool has_arg { get; set; }

        public OpCode (uint8 value, string doc = "No documentation", bool has_arg = false) {
            this.documentation = doc;
            this.value = value;
            this.has_arg = has_arg;
        }

        public uint8[] to_bytecode (uint16? arg = null) {
            if (arg != null) {
                if (this.has_arg) {
                    return { this.value, (uint8)(arg & 0xff), (uint8)((arg >> 8) & 0xff) };
                } else {
                    log_error ("Compiler tried to use an argument with the 0x%02hhx OpCode, which is forbidden.".printf (this.value));
                    return { this.value };
                }
            } else {
                if (this.has_arg) {
                    log_error ("Compiler forgotten an argument for the 0x%02hhx OpCode.".printf (this.value));
                    return { this.value, 0x00, 0x00 };
                } else {
                    return { this.value };
                }
            }
        }

    }
}
