The only instruction supported yet is the addition of two literals numbers.

```
42 + 7
```

For instance, this code will output:

```
50 55 4c 50                                             PULP
00 01 00                                                0.1.0
00                                                      padding
00 00 00 00 58 0c 63 a4                                 timestamp
20 5f 66 76 7f 14 ed e9 0f c8 e4 45 2a c3 e8 93 00 40   MD5

2d 00 00 00                                             A segment begins
01                                                      Type = code
00 00 00                                                padding

04 00 00 00                                             Empty symbols table

16 00 02 00                                             Constants table
  01 2a 00 00 00 00 00 00 00 00                         42
  01 07 00 00 00 00 00 00 00 00                         7

0b 00 03 00                                             Code
0a 00 00                                                LOAD_CONST 0 (Stack = [42])
0a 01 00                                                LOAD_CONST 1 (Stack = [7, 42])
30                                                      ADD          (Stack = [49])
```
