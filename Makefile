GEE = gee-0.8
LIB = --pkg $(GEE) --pkg gio-2.0
OUT = pulpc
FILES = *.vala ast/*.vala
C-WARNINGS = -Wno-incompatible-pointer-types

all:
	@valac $(LIB) $(FILES) -o $(OUT) -X $(C-WARNINGS)
	@echo
	@echo
	./$(OUT) test.pulp -o test --debug
	@echo
	@echo "Output:"
	@echo
	@od -t x1 test

windows:
	# TODO ; use gee-1.0 on windows
	valac $(LIB) $(FILES) -o $(OUT)
	$(OUT)

# useless, but funny
linecount:
	@printf $$(cat $(FILES) | sed '/^\s*$$/d' | wc -l)
	@echo ' non-empty lines in the whole project.'
