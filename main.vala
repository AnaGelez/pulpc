using Pulp.Compiler.AST;

namespace Pulp.Compiler {
    public class Main : Object {
        const string VERSION = "0.1.0";

        // options
        static bool version;
        [CCode (array_length = false, array_null_terminated = true)]
        static string[] sources;

        static string? output = null;

        public static bool debug;

        const OptionEntry[] options = {
            { "", 0, 0, OptionArg.FILENAME_ARRAY, ref sources, null, "file.pulp" },
            { "version", 0, 0, OptionArg.NONE, ref version, null, "Display the version number" },
            { "output", 'o', 0, OptionArg.FILENAME, ref output, null, "The path of the output file" },
            { "debug", 0, 0, OptionArg.NONE, ref debug, null, "Logs debugging informations during the compilation" },
            { null } // I love C.
        };

        /*
        * The entry point
        *
        * It parses options and configure the compiler
        */
        public static int main (string[] args) {
    		try {
    			var opt_context = new OptionContext ("- pulp Compiler");
    			opt_context.set_help_enabled (true);
    			opt_context.add_main_entries (options, null);
    			opt_context.parse (ref args);
    		} catch (OptionError e) {
    			log_error ("%s\n".printf (e.message));
    			log_info ("Run '%s --help' to see a full list of available command line options.\n".printf (args[0]));
    			return 1;
    		}

    		if (version) {
    			print ("puplc %s\n", VERSION);
    			return 0;
    		}

    		if (sources == null) {
    			log_error ("No source file specified.\n");
    			return 1;
    		}

    		// We compile

            new Repository (); // the wonderful gobject world... if I don't do that, i can't use static properties of Repository
            Compiler cp = new Compiler (sources, output == null ? sources[0].split(".")[sources[0].split(".").length - 1] : output);
            cp.compile ();

            if (error_count > 0) {
                log_info ("Compilation failed - %d error%s.".printf (error_count, (error_count == 1 ? "" : "s")));
                return 1;
            } else {
                log_info ("Compilation succeed");
                return 0;
            }
        }
    }
}
