namespace Pulp.Compiler {

    const string ANSI_COLOR_RED = "\x1b[31m";
    const string ANSI_COLOR_GREEN = "\x1b[32m";
    const string ANSI_COLOR_YELLOW = "\x1b[33m";
    const string ANSI_COLOR_BLUE = "\x1b[34m";
    const string ANSI_COLOR_MAGENTA = "\x1b[35m";
    const string ANSI_COLOR_CYAN = "\x1b[36m";
    const string ANSI_COLOR_RESET = "\x1b[0m";

    public static int error_count = 0;

    /**
    * Helper method to log errors
    */
    public static void log_error (string error, int? line = null, int? col = null) {
        error_count++;
        string msg = @"$(ANSI_COLOR_RED)[ERROR]$(ANSI_COLOR_RESET) ";
        if (line != null) {
            msg += "line " + line.to_string () + (col != null ? ", ": " : ");
        }
        if (col != null) {
            msg += "column " + col.to_string () + " : ";
        }
        stderr.printf ("%s%s\n\n", msg, error);
    }

    public static void log_warning (string warning, int? line = null, int? col = null) {
        string msg = @"$(ANSI_COLOR_YELLOW)[WARNING]$(ANSI_COLOR_RESET) ";
        if (line != null) {
            msg += "line " + line.to_string () + (col != null ? ", ": " : ");
        }
        if (col != null) {
            msg += "column " + col.to_string () + " : ";
        }
        print ("%s%s\n\n", msg, warning);
    }

    public static void log_info (string info) {
        print (@"$(ANSI_COLOR_BLUE)[INFO]$(ANSI_COLOR_RESET) %s\n\n", info);
    }

}
