using Gee;

namespace Pulp.Compiler {
    public class Compiler : Object {

        private string[] files { get; set; }
        private string output { get; set; }

        public Writer writer { get; set; default = new Writer(); }

        public Compiler (string[] files, string output) {
            this.files = files;
            this.output = output;
        }

        public void compile () {
            foreach (string file in this.files) {
                try {
                    string content;
                    FileUtils.get_contents (file, out content);

                    Writer wr = new Writer ();

                    var tokens = new Tokenizer ().tokenize (content);
                    if (error_count != 0) {
                        return;
                    }

                    var parsedCode = new Parser ().parse (tokens);

                    if (error_count != 0) {
                        return;
                    }

                    if (Main.debug) {
                        log_info ("Generated AST :");
                        print (parsedCode.to_string ());
                    }

                    parsedCode.write_bytecode (wr);

                    if (error_count != 0) {
                        return;
                    }

                    File @out = File.new_for_path (this.output);
                    if (@out.query_exists ()) {
                        @out.delete ();
                    }

                    FileIOStream ios = @out.create_readwrite (FileCreateFlags.REPLACE_DESTINATION);
            		DataOutputStream dos = new DataOutputStream (ios.output_stream);

                    foreach (uint8 byt in "PULP".data) {
                        dos.put_byte (byt);
                    }
                    // TODO : dos add padding when it writes timestamp
                    dos.put_byte ((uint8) VERSION_MAJOR);
                    dos.put_byte ((uint8) VERSION_MINOR);
                    dos.put_byte ((uint8) VERSION_PATCH);
                    dos.put_byte ((uint8)0x00); // padding
                    int64 timestamp = new DateTime.now_utc ().to_unix ();
                    dos.put_int64 (timestamp);

                    Checksum cs = new Checksum (ChecksumType.MD5);
                    cs.update (wr.segment ().to_array (), (size_t)wr.segment ().size);
                    uint8[] buff = new uint8[16];
                    size_t size = 16;
                    cs.get_digest (buff, ref size);
                    foreach (uint8 byt in buff) {
                        dos.put_byte (byt);
                    }

                    foreach (uint8 byt in wr.segment ()) {
                        dos.put_byte (byt);
                    }

                } catch (Error err) {
                    log_error (err.message);
                }
            }
        }
    }
}
