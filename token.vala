namespace Pulp.Compiler {

    /*
    * Represents a token of code.
    */
    public class Token : Object {

        /*
        * Creates a new token with the specified lexeme and category.
        */
        public Token (string lex, string cat, int line = 0, int col = 0) {
            this.lexeme = lex;
            this.category = cat;
            this.line = line;
            this.column = col;
        }

        /*
        * The value in the pulp code of this token.
        *
        * For instance, you can have `42`, `+` or `=`.
        */
        public string lexeme { get; set; }

        /*
        * The category of this token.
        *
        * For instance, you can have `literal.number` or `operator.add`.
        */
        public string category { get; set; }

        public int line { get; set; }

        public int column { get; set; }
    }
}
