namespace Pulp.Compiler {
    public interface Writable : Object {
        public abstract uint8[] to_bytecode (uint8? arg);
    }
}
