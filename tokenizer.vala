using Gee;
using Pulp.Compiler.AST;

namespace Pulp.Compiler {
    public class Tokenizer : Object {

        //const Regex re_int = new Regex ("\\d+");

        public ArrayList<Token> tokenize (string src) {
            ArrayList<string> exprs = new ArrayList<string> ();
            ArrayList<int> lines = new ArrayList<int> ();
            ArrayList<int> cols = new ArrayList<int> ();
            ArrayList<Token> res = new  ArrayList<Token> ();

            // we split each expression
            bool in_num = false, in_str = false, in_comment = false;
            int line_count = 1, col_count = 0;
            foreach (char _ch in src.to_utf8 ()) {
                string ch = _ch.to_string ();
                col_count++;

                // ignore comments
                if (ch == "#") {
                    in_comment = true;
                } else if (ch == "\n") {
                    in_comment = false;
                }
                if (in_comment) {
                    continue;
                }

                if (ch == "\n") {
                    exprs.add ("\n");
                    lines.add (line_count);
                    cols.add (col_count);
                    col_count = 0;
                    line_count++;
                }

                // string literals
                if (in_str) {
                    exprs[exprs.size - 1] += ch;
                }
                if (ch == "'") {
                    in_str = !in_str;
                    if (in_str) {
                        exprs.add ("'");
                        lines.add (line_count);
                        cols.add (col_count);
                    }
                }
                if (in_str) {
                    continue;
                }

                // numbers
                if (int64.try_parse (ch)) {
                    if (in_num) {
                        exprs[exprs.size - 1] = (exprs[exprs.size - 1]) + ch;
                    } else {
                        exprs.add (ch);
                        lines.add (line_count);
                        cols.add (col_count);
                    }
                    in_num = true;
                } else {
                    in_num = false;
                }

                // + operator
                if (ch == "+") {
                    exprs.add ("+");
                    lines.add (line_count);
                    cols.add (col_count);
                }

                // - operator
                if (ch == "-") {
                    exprs.add ("-");
                    lines.add (line_count);
                    cols.add (col_count);
                }
            }
            if (in_str) {
                log_error ("Unterminated string literal.");
            }

            // we create the token corresponding to each expression
            int i = 0;
            foreach (string expr in exprs) {
                int64 num;
                if (int64.try_parse (expr, out num)) {
                    res.add (new Token (expr, "literal.number", lines[i], cols[i]));
                }

                if (expr == "+") {
                    res.add (new Token (expr, "operator.add", lines[i], cols[i]));
                }

                if (expr == "-") {
                    res.add (new Token (expr, "operator.sub", lines[i], cols[i]));
                }

                if (expr.has_prefix ("'") && expr.has_suffix ("'")) {
                    res.add (new Token (expr, "literal.string", lines[i], cols[i]));
                }

                if (expr == "\n") {
                    res.add (new Token (expr, "expression.end", lines[i], cols[i]));
                }
                i++;
            }
            return res;
        }

    }
}
