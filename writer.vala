using Gee;
using Pulp.Compiler.AST;

namespace Pulp.Compiler {
    public class Writer : Object {

        private ArrayList<uint8> bytecode { get; set; default = new ArrayList<uint8> (); }
        public ArrayList<Constant> constTable { get; set; default = new ArrayList<Constant> (); }
        public ArrayList<string> symbolTable { get; set; default = new ArrayList<string> (); }

        public int16 opCount = 0;

        public void puts_opcode (OpCode code, uint16? arg = null) {
            this.opCount++;
            foreach (uint8 byt in code.to_bytecode (arg)) {
                this.puts (byt);
            }
        }

        public void puts_int64 (int64 int_to_write) {
            this.puts ((uint8)(int_to_write >> 0) & 0xff);
            this.puts ((uint8)(int_to_write >> 8) & 0xff);
            this.puts ((uint8)(int_to_write >> 16) & 0xff);
            this.puts ((uint8)(int_to_write >> 24) & 0xff);
            this.puts ((uint8)(int_to_write >> 32) & 0xff);
            this.puts ((uint8)(int_to_write >> 40) & 0xff);
            this.puts ((uint8)(int_to_write >> 48) & 0xff);
            this.puts ((uint8)(int_to_write >> 56) & 0xff);
        }

        public void puts_int (int int_to_write) {
            this.puts ((uint8)(int_to_write >> 0) & 0xff);
            this.puts ((uint8)(int_to_write >> 8) & 0xff);
            this.puts ((uint8)(int_to_write >> 16) & 0xff);
            this.puts ((uint8)(int_to_write >> 24) & 0xff);
        }

        public void puts (uint8 byte_to_write) {
            this.bytecode.add (byte_to_write);
        }

        /*
        * Gives the bytecode of the segment
        */
        public ArrayList<uint8> segment () {
            ArrayList<uint8> res = new ArrayList<uint8> ();
            const int HEADER_SIZE = 8;

            int16 constSize = 4; // min size for constTable is 4 bytes (2 bytes `size` + 2 bytes `length`)
            foreach (Constant cst in this.constTable) {
                constSize += (int16)(1 + cst.value.length);
            }

            int16 symbSize = 4; // min size for symbtable
            foreach (string symb in this.symbolTable) {
                symbSize += (int16)(1 + symb.data.length);
            }

            int16 codeSize = 4 + (int16)this.bytecode.size;

            int segment_size = HEADER_SIZE + constSize + symbSize + codeSize;

            // segment
            //     -> header
            //         -> size
            res.add ((uint8)(segment_size & 0xff));
            res.add ((uint8)(segment_size >> 8) & 0xff);
            res.add ((uint8)(segment_size >> 16) & 0xff);
            res.add ((uint8)(segment_size >> 24) & 0xff);
            //         -> type
            res.add (0x01);
            //         -> padding
            res.add (0x00);
            res.add (0x00);
            res.add (0x00);

            //     -> symbolTable
            //         -> size
            res.add ((uint8)(symbSize & 0xff));
            res.add ((uint8)(symbSize >> 8) & 0xff);
            //         -> count
            res.add ((uint8)(this.symbolTable.size & 0xff));
            res.add ((uint8)(this.symbolTable.size >> 8) & 0xff);
            //         -> elements
            foreach (string symb in this.symbolTable) {
                res.add ((uint8)(symb.data.length));
                foreach (uint8 byt in symb.data) {
                    res.add (byt);
                }
            }

            //     -> constTable
            //         -> size
            res.add ((uint8)(constSize & 0xff));
            res.add ((uint8)(constSize >> 8) & 0xff);
            //         -> count
            res.add ((uint8)(this.constTable.size & 0xff));
            res.add ((uint8)(this.constTable.size >> 8) & 0xff);
            //         -> elements
            foreach (Constant cst in this.constTable) {
                res.add_all (cst.to_bytecode ());
            }

            //     -> code
            //         -> size
            res.add ((uint8)(codeSize & 0xff));
            res.add ((uint8)(codeSize >> 8) & 0xff);
            //         -> count
            res.add ((uint8)(this.opCount & 0xff));
            res.add ((uint8)(this.opCount >> 8) & 0xff);

            res.add_all (this.bytecode);

            return res;
        }

        public void clear () {
            this.bytecode.clear ();
        }

    }
}
