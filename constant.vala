using Gee;

namespace Pulp.Compiler {
    public class Constant : Object {

        public uint8 data_type { get; set; default = 0x01; }

        public uint8[] value { get; set; }

        public ArrayList<uint8> to_bytecode () {
            ArrayList<uint8> res = new ArrayList<uint8> ();
            res.add (data_type);
            foreach (uint8 byt in this.value) {
                res.add (byt);
            }
            return res;
        }

    }
}
