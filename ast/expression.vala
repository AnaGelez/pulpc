namespace Pulp.Compiler.AST {
    public interface Expression : Object {

        public abstract string to_string ();

        public abstract void write_bytecode (Writer wr);

    }
}
