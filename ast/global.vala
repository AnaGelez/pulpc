using Gee;

namespace Pulp.Compiler.AST {

    /*
    * The root Expression of the source code.
    *
    * It contains a list of others expressions.
    */
    public class Global : Object, Expression {

        public ArrayList<Expression> children { get; set; default = new ArrayList<Expression> (); }

        public string to_string () {
            string res = "[\n";
            foreach (Expression child in this.children) {
                res += @"{\n$(child)\n}";
            }
            res += "\n]\n";
            return res;
        }

        public void write_bytecode (Writer wr) {
            foreach (Expression child in this.children) {
                child.write_bytecode (wr);
            }
        }

    }
}
