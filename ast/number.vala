namespace Pulp.Compiler.AST {

    public class Number : Object, Expression, Literal {

        public int64 value { get; set; }

        public string to_string () {
            return this.value.to_string ();
        }

        public void write_bytecode (Writer wr) {
            wr.constTable.add(new Constant () {
                value = {
                    (uint8)(this.value & 0xff), (uint8)((this.value >> 8) & 0xff),
                    (uint8)((this.value >> 16) & 0xff), (uint8)((this.value >> 24) & 0xff),
                    (uint8)((this.value >> 32) & 0xff), (uint8)((this.value >> 40) & 0xff),
                    (uint8)((this.value >> 48) & 0xff), (uint8)((this.value >> 56) & 0xff)
                }
            });
            wr.puts_opcode (Repository.LOAD_CONST, (int16)(wr.constTable.size - 1));
        }
    }

}
