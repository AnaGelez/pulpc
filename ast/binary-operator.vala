using Gee;

namespace Pulp.Compiler.AST {

    public class BinaryOperator : Object, Expression {

                            // operator, priority
        public static HashMap<string, int> registered_operators { get; set; }

        public BinaryOperator () {
            if (registered_operators == null) {
                registered_operators = new HashMap<string, int> ();
                registered_operators["+"] = 0;
                registered_operators["-"] = 0;
                registered_operators["*"] = 1;
                registered_operators["/"] = 1;
                registered_operators["^"] = 2;
            }
        }

        public Expression left { get; set; }

        public Expression right { get; set; }

        private string _operator;
        public string operator {
            get {
                return _operator;
            }
            set {
                if (registered_operators.has_key (value)) {
                    this._operator = value;
                }
            }
        }

        public string to_string () {
            return @"$(this.operator) : [\n\t{\n\t\t$(this.left)\n\t}, {\n\t\t$(this.right)\n\t}\n]\n";
        }

        public void write_bytecode (Writer wr) {
            this.right.write_bytecode (wr);
            this.left.write_bytecode (wr);
            switch (this.operator) {
                case "+":
                    wr.puts_opcode (Repository.ADD);
                    break;
                case "-":
                    wr.puts_opcode (Repository.SUB);
                    break;
                default:
                    log_error ("Unsupported binary operator '%s'".printf (this.operator));
                    break;
            }
        }

    }

}
